import {createElement} from '../helpers/domHelper';
import {showModal} from './modal';

export function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({title, bodyElement});
}

function createFighterDetails(fighter) {
  const {name, attack, defense, health, source} = fighter;

  const fighterDetails = createElement({tagName: 'div', className: 'modal-body'});
  const imageElement = createElement({tagName: 'img', className: 'fighter-details-image', attributes: {src: source}});
  const nameElement = createElement({tagName: 'span', className: 'fighter-details-name'});
  const details = createElement({tagName: 'div', className: 'fighter-details-row'})
  const healthElement = createElement({tagName: 'span', className: 'fighter-details-health'});
  healthElement.className += ' fighter-details-characteristic';
  const attackElement = createElement({tagName: 'span', className: 'fighter-details-attack'});
  attackElement.className += ' fighter-details-characteristic';
  const defenseElement = createElement({tagName: 'span', className: 'fighter-details-defence'});
  defenseElement.className += ' fighter-details-characteristic';

  nameElement.innerText = name;
  healthElement.innerText = `❤️ ${health}`;
  attackElement.innerText = `⚔️ ${attack}`;
  defenseElement.innerText = `🛡️ ${defense}`;

  details.append(healthElement, attackElement, defenseElement);

  fighterDetails.append(imageElement, nameElement, details);

  return fighterDetails;
}
