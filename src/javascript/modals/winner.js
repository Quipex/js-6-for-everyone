import {showModal} from "./modal";
import {createElement} from "../helpers/domHelper";

export function showWinnerModal(fighter) {
  const winner = `${fighter.name} is the winner!`;
  console.log(winner)
  const bodyElement = createElement({tagName: 'img', className: 'fighter-details-image'});
  bodyElement.src = fighter.source;
  showModal({title: winner, bodyElement})
}
