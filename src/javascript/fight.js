/*
Игроки наносят удари друг другу по очереди, а уровень их health уменьшается на getDamage. Если один из них умирает,
то игра заканчивается и функция должна вернуть победителя. Имя победителя выводится на экран с помощью функции
showWinnerModal.
 */

export function fight(firstFighter, secondFighter) {
  let firstHP = firstFighter.health;
  let secondHP = secondFighter.health;
  let turnOfFirst = true;
  while (firstHP > 0 && secondHP > 0) {
    if (turnOfFirst) {
      secondHP -= getDamage(firstFighter, secondFighter);
    } else {
      firstHP -= getDamage(secondFighter, firstFighter);
    }
    turnOfFirst = !turnOfFirst;
  }
  return firstHP > 0 ? firstFighter : secondFighter;
}

export function getDamage(attacker, enemy) {
  const hit = getHitPower(attacker);
  const blocked = getBlockPower(enemy);
  const damage = hit - blocked;
  return damage < 0 ? 0 : damage;
}

export function getHitPower(fighter) {
  return fighter.attack * getRandomChance();
}

export function getBlockPower(fighter) {
  return fighter.defense * getRandomChance();
}

function getRandomChance() {
  // random number between 1 inclusive and 2 exclusive
  return Math.random() + 1;
}
