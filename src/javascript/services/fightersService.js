import {callApi} from '../helpers/apiHelper';

export async function getFighters() {
  return await get('fighters.json');
}

export async function getFighterDetails(id) {
  return await get(`details/fighter/${id}.json`);
}

async function get(endpoint) {
  try {
    return await callApi(endpoint, 'GET');
  } catch (error) {
    throw error;
  }
}
